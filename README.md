# Getting Started with PyQt5 #

This repository contains code examples for the associated Wiki. See the [Wiki](../../wiki/Home) page for the documents that describe how to get started with PyQt5.

## Licenses ##

Most of the examples in the code repository are licensed under a [modified BSD license](http://directory.fsf.org/wiki/License:BSD_3Clause). However, some very short examples are licensed under the [GNU All-Permissive License](http://www.gnu.org/prep/maintain/html_node/License-Notices-for-Other-Files.html). Each file contains the relevant license text.