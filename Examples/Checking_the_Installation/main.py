#!/usr/bin/env python3

# main.py - Shows the Qt version number.
#
# Copyright (C) 2015 David Boddie <david@boddie.org.uk>
#
# This file is part of Getting Started With PyQt5.
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

from PyQt5.QtCore import QT_VERSION_STR
print(QT_VERSION_STR)
