#!/usr/bin/env python3

# hello.py - Shows a push button.
#
# Copyright (C) 2015 David Boddie <david@boddie.org.uk>
#
# This file is part of Getting Started With PyQt5.
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

import sys
from PyQt5.QtWidgets import QApplication, QPushButton

app = QApplication(sys.argv)
button = QPushButton("Hello world!")
button.show()
sys.exit(app.exec_())
