==========================
Getting Started with PyQt5
==========================

:Author: `David Boddie`_ and contributors
:Date: 2015-06-14

*Note: The source text is marked up using reStructuredText formatting. It should be readable in a text editor but can be processed to produce versions of this document in other formats.*

.. contents::

Introduction
------------

This Wiki is intended to be a collection of documents that introduce developers to `PyQt5`_, accompanying the examples contained in the associated code repository. The reader is expected to have an understanding of the Python_ language and Python 3 in particular.

Contents
--------

 * `Installing PyQt5`_
 * `Checking the Installation`_
 * `Overview of PyQt5`_
 * `Creating an Application`_
 * `Widgets and Layouts`_

Reporting Errors and Updating the Material
------------------------------------------

If you find error in either the text or examples, please try to submit a patch that fixes it. Both the text and the examples are stored in Mercurial repositories that can be cloned either to a new repository on Bitbucket or to a repository on your own system. Bitbucket provides a mechanism that allows you to request that I pull your changes into my repository. Alternatively, if you publish your repository elsewhere and send me (`David Boddie`_) an e-mail about your changes, I can pull them from their published location.

Licenses
--------

The text in this Wiki is licensed under the `Creative Commons Attribution-ShareAlike 4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`_.

Most of the examples in the code repository are licensed under a `modified BSD license`_. However, some very short examples are licensed under the `GNU All-Permissive License`_. Each file contains the relevant license text.

The full text for these licenses can be found in the code repository.

----

|CC-BY-SA| Getting Started with PyQt5 by David Boddie and contributors is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License`_. 

.. |CC-BY-SA| image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png 
.. _`Creative Commons Attribution-ShareAlike 4.0 International License`: http://creativecommons.org/licenses/by-sa/4.0/
.. _PyQt5:                  http://www.riverbankcomputing.com/software/pyqt/
.. _Python:                 http://www.python.org/
.. _`David Boddie`:         mailto:david@boddie.org.uk
.. _`Installing PyQt5`:     Installing_PyQt5
.. _`Checking the Installation`: Checking_the_Installation
.. _`Overview of PyQt5`:    Overview_of_PyQt5
.. _`Creating an Application`: Creating_an_Application
.. _`Widgets and Layouts`:  Widgets_and_Layouts
.. _`GNU All-Permissive License`: http://www.gnu.org/prep/maintain/html_node/License-Notices-for-Other-Files.html
.. _`modified BSD license`: http://directory.fsf.org/wiki/License:BSD_3Clause
