`Home`_ | Previous: `Checking the Installation`_ | Next: `Widgets and Layouts`_

=================
Overview of PyQt5
=================

Introduction
============

PyQt5 is a set of Python bindings to Qt 5, a framework which aims to make
development of cross-platform applications straightforward. Traditionally, Qt
has supported the kinds of applications found on desktop systems: PCs, Macs and
Linux/Unix workstations. However, since the release of Qt 4.7, there has been
increased focus on development of applications for mobile, embedded and
automotive applications, reflecting the adoption of Qt within companies in those
areas. Nonetheless, Qt is still used to create desktop applications, especially
in areas where users need to perform complex tasks, or where rich, complex
controls are needed. As a result, PyQt5 is also well-suited to creating these
kinds of applications.

The shift towards mobile-style user interfaces is reflected in the distribution
of features between modules in PyQt5 compared to PyQt4. For instance, for those
developers used to PyQt4, it is useful to know that many of classes that used to
be found in the ``QtGui`` module can now be found in the ``QtWidgets`` module.
The details of differences between PyQt4 and PyQt5 can be found in the PyQt5
documentation: `Differences Between PyQt4 and PyQt5`_. Although the use of
traditional widgets is not something many mobile application developers are
interested in, we will concentrate on this part of PyQt5 in many of the
documents in this Wiki as we explore the creation of desktop applications.

Qt also provides a number of other features that make life easier for
developers, and these are available in PyQt5. Not all of these are necessarily
relevant to Python programmers, though it may be preferable to use the modules
PyQt5 provides for SQL, networking and XML handling in certain cases where close
integration with the graphical user interface (GUI) is desired.

Concepts
========

It is important to realize that, unlike many modules available for Python, the
modules provided by PyQt5 are part of a *framework* which coexists with the
developer's application code. Although the application is in control to start
with, it almost immediately gives up control to the framework, and subsequently
is only really responsible for *handling events*. Programmers who are used to
writing applications where they control the flow of execution, calling out to
modules to perform tasks and managing events, may find it takes time to get used
to the way that control is inverted in PyQt applications.

In Python programs, the developer does not normally need to worry about memory
management. The reference counting used in standard Python implementations means
that objects are not deleted as long as some part of the application holds a
reference to it. As a result, many Python modules and frameworks are designed
with this property in mind. In contrast, Qt is designed with the C++ language in
mind and deals with allocations of objects differently. Qt manages *ownership of
objects* by establishing parent-child relationships between objects and uses
convention to determine who is responsible for deleting them. One advantage of
this is that the application does not need to maintain references to certain
anonymous objects. The disadvantage is that there are circumstances in which the
framework may delete an object, leaving the application with an invalid
reference. Fortunately, this is not always harmful, and will not lead to
anything more than a warning, but there are situations where object
mismanagement can cause the framework, and therefore the application, to crash.
We will try to warn about this when we discuss areas of PyQt5 where this type of
problem can occur.

Signals and slots

Features
========

The most prominent feature of PyQt5 is its set of `graphical user interface
widgets`_ that mostly follow the style of the platforms that Qt supports. These
are often the main reason why developers choose to investigate PyQt5. The core
set of widgets have remained stable for many years and are now more-or-less
fixed. This reflects the general situation of desktop environments, however, and
where new widgets need to be made they can be constructed using the basic
building blocks that the framework provides.

More complex widgets are also provided as part of the ``QtWidgets`` module,
including those used to display lists, tables and trees of items. These use a
`model-view-controller`_ style mechanism to allow data to be displayed and
edited. Since it can be require a lot of effort to design a model for these
widgets, a set of simplified models is provided that are often sufficient for
many applications.

Another old but stable feature is a `canvas`_ that can be used for more highly
interactive applications that display custom content. This makes it easier to
put items of information in front of the user in a free-form environment that is
suited to use in visualization tools.

----

|CC-BY-SA| Getting Started with PyQt5 by David Boddie and contributors is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License`_. 

.. |CC-BY-SA| image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png 
.. _`Creative Commons Attribution-ShareAlike 4.0 International License`: http://creativecommons.org/licenses/by-sa/4.0/
.. _Home: Home
.. _`Checking the Installation`:            Checking_the_Installation
.. _`Installing PyQt5`:                     Installing_PyQt5
.. _`Differences Between PyQt4 and PyQt5`:  http://pyqt.sourceforge.net/Docs/PyQt5/pyqt4_differences.html
.. _`graphical user interface widgets`:     Widgets_and_Layouts
.. _`model-view-controller`:                Models_and_Views
.. _`canvas`:                               Graphics_View
.. _`Widgets and Layouts`:                  Widgets_and_Layouts
