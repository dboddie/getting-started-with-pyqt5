`Home`_ | Next: `Checking the Installation`_

================
Installing PyQt5
================

Since PyQt5 is available for a variety of platforms, there is more than one way to install it. Follow the instructions for your chosen platform below.

.. contents::

GNU/Linux
=========

Different families of GNU/Linux distributions use different packaging systems to provide software. Since PyQt5 is used by both developers and users, it is typically split into multiple packages, some of which provide the resources needed by applications while others contain the tools used by developers. We provide the instructions needed to install the packages developers need in order to create new applications. Where additional tools are available in a separate package, we indicate which package is needed and the tools it contains.

Debian
------

PyQt5 is available as a collection of packages for Debian 8 (Jessie). To install a basic set of PyQt5 modules, you need to install the ``python3-pyqt5`` package. You can use a GUI package manager to do this, or run the following in a console:

  sudo apt-get install python3-pyqt5

Additional modules are provided by packages with names that begin with ``python3-pyqt5``, such as ``python3-pyqt5.qtsql`` for the SQL database integration module. These can be installed in the same way:

  sudo apt-get install python3-pyqt5.qtsql

If you find that you are missing functionality, or have obtained code from somewhere which fails due to a missing module, it is worth checking the package manager to see if you need to install one of these packages. You can also obtain a list of relevant packages by running the following from the console:

  apt-cache search python3-pyqt5.*

Some additional tools are needed if you intend to generate translations of your software for use with Qt Linguist (``pylupdate5``), generate code from UI files created by Qt Designer (``pyuic5``) or embed assets or resources into Python code (``pyrcc5``). These tools are provided by the ``pyqt5-dev-tools`` package:

  sudo apt-get install pyqt5-dev-tools

Installing from source
----------------------

If your distribution does not have appropriate packages for PyQt5, or you need to use a later version than the one provided, you will probably need to build the software yourself. Download the latest releases of PyQt5 from the `PyQt5 download page`_ and SIP from the `SIP download page`_, making sure that you get the `.tar.gz` archives instead of the `.zip` archives. Unpack them and follow the instructions provided to build and install the software.

----

|CC-BY-SA| Getting Started with PyQt5 by David Boddie and contributors is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License`_. 

.. |CC-BY-SA| image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png 
.. _`Creative Commons Attribution-ShareAlike 4.0 International License`: http://creativecommons.org/licenses/by-sa/4.0/
.. _Home: Home
.. _`Checking the Installation`: Checking_the_Installation
.. _`PyQt5 download page`: http://www.riverbankcomputing.com/software/pyqt/download5
.. _`SIP download page`: http://www.riverbankcomputing.com/software/sip/download
