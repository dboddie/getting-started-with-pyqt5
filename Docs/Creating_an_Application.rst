`Home`_ | Previous: `Overview of PyQt5`_ | Next: `Widgets and Layouts`_

=======================
Creating an Application
=======================

In PyQt5, applications are Python programs that create an instance of an
application class. There is more than one of these classes in PyQt5, but they
are all based on the ``QCoreApplication`` class from the ``QtCore`` module.
However, for widget-based applications that many developers are creating, the
class to use is ``QApplication``, which can be found in the ``QtWidgets``
module.

One of the simplest way to use ``QApplication`` can be seen in the following
example:

.. sourcecode :: python

  import sys
  from PyQt5.QtWidgets import QApplication, QPushButton

  app = QApplication(sys.argv)
  button = QPushButton("Hello world!")
  button.show()
  result = app.exec_()
  sys.exit(result)

We can see that we use the ``sys`` module and two classes from the ``QtWidgets``
module. We create an instance of ``QApplication`` using the list of command line
arguments provided by the ``sys.argv`` variable. Behind the scenes, the
framework inspects the arguments for certain options but that is not relevant
here.

Although the application has been created, it is not actually doing
anything yet. Before we get it up and running, we take the opportunity to
create a ``QPushButton`` widget with the text, "Hello world!" Then we call its
``show()`` method to display it, but note that it does not immediately appear.

Finally, on the final two lines, we run the application by calling its
``exec_()`` method then exit Python with that value. When the application exits,
this method will return a value indicating how it exited. Often, we are not
interested in this value, so the last two lines can be merged into

.. sourcecode :: python

  sys.exit(app.exec_())

In a console, enter the ``Examples/Creating_an_Application`` directory and run
the example:

  python3 hello.py

You should see a window appear containing the text, "Hello world!" Closing the
window will cause the application to exit. This is built-in behaviour that can
be overridden, but most applications rely on the behaviour that closing the
last application window closes the application itself.


----

|CC-BY-SA| Getting Started with PyQt5 by David Boddie and contributors is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License`_. 

.. |CC-BY-SA| image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png 
.. _`Creative Commons Attribution-ShareAlike 4.0 International License`: http://creativecommons.org/licenses/by-sa/4.0/
.. _Home: Home
.. _`Overview of PyQt5`:                    Overview_of_PyQt5
.. _`Widgets and Layouts`:                  Widgets_and_Layouts
.. _`model-view-controller`:                Models_and_Views
