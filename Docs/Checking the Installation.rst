`Home`_ | Previous: `Installing PyQt5`_ | Next: `Overview of PyQt5`_

=========================
Checking the Installation
=========================

Once you have installed PyQt5 on your development system, you can check it by running the `Checking_the_Installation example`_.

In a console, enter the ``Examples/Checking_the_Installation`` directory and run the example:

  python3 main.py

The example should print a single line containing the version of Qt that PyQt5 uses on your system. For instance, running the example on Debian 8 (Jessie) produces the following output:

  5.3.2

If you do not obtain a simple version number, or if you obtain an error, then check that you have followed the installation instructions for your system given in the `Installing PyQt5`_ document. If you have done everything mentioned in that document, you may find it useful to see if the cause of your installation problem is mentioned in the section below.

Troubleshooting
---------------

There are a number of reasons why you may encounter a problem running the above example if your installation of PyQt5 is incomplete or broken. The following errors and explanations aim to help you determine the cause.

Not installed or installed in an unknown location
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you obtain the following output when running the example then either PyQt5 is not installed at all, or it is installed in a location that Python does not know about:

  Traceback (most recent call last):
    File "Examples/Checking_the_Installation/main.py", line 14, in <module>
      from PyQt5.QtCore import QT_VERSION_STR
  ImportError: No module named 'PyQt5'

----

|CC-BY-SA| Getting Started with PyQt5 by David Boddie and contributors is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License`_. 

.. |CC-BY-SA| image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png 
.. _`Creative Commons Attribution-ShareAlike 4.0 International License`: http://creativecommons.org/licenses/by-sa/4.0/
.. _Home: Home
.. _`Installing PyQt5`: Installing_PyQt5
.. _`Checking_the_Installation example`: ../src/tip/Examples/Checking_the_Installation/main.py
.. _`Overview of PyQt5`: Overview_of_PyQt5
