`Home`_ | Previous: `Creating an Application`_ | Next: `Widgets and Layouts`_

===================
Widgets and Layouts
===================

Widgets
=======

In PyQt5, GUI applications are constructed from building blocks called widgets.
These are used to create all the elements of the user interface, from containers
such as windows to specialized controls that the user interacts with. This
common foundation gives the developers a lot of flexibility in the way they can
construct their user interface, and it also makes construction of new widgets
possible, and these will be treated just like the standard ones.

The classes for widgets

----

|CC-BY-SA| Getting Started with PyQt5 by David Boddie and contributors is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License`_. 

.. |CC-BY-SA| image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png 
.. _`Creative Commons Attribution-ShareAlike 4.0 International License`: http://creativecommons.org/licenses/by-sa/4.0/
.. _Home: Home
.. _`Creating an Application`:              Creating_an_Application
.. _`model-view-controller`:                Models_and_Views
